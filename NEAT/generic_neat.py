#!/usr/bin/env python
# coding: utf-8


import pygame
import random
import os
import time
import neat
import numpy as np
import FlappyBird as fb





WINDOW_W = fb.WINDOW_W
WINDOW_H = fb.WINDOW_H
Bird = fb.Bird
Pipe = fb.Pipe
Base = fb.Base
F_BASE = fb.F_BASE

gen  = 0
win = pygame.display.set_mode((WINDOW_W, WINDOW_H))
def flappy_fitness(genomes,config):
    global gen , win

    gen += 1

    nns = []
    birds = []
    ge = []
    for g_id, genome in genomes:
        genome.fitness = 0  # start with fitness level of 0
        nn = neat.nn.FeedForwardNetwork.create(genome, config)
        nns.append(nn)
        birds.append(Bird(WINDOW_W/4,WINDOW_H/2))
        ge.append(genome)

    base = Base(F_BASE)
    pipes = [Pipe(WINDOW_W)]
    score = 0

    clock = pygame.time.Clock()

    run = True
    while run and len(birds) > 0:
        clock.tick(30)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
                quit()
                break

        pipe_idx = 0
        if len(birds) > 0:
            if len(pipes) > 1 and birds[0].pos_x > pipes[0].x + pipes[0].P_TOP.get_width():  
                pipe_idx = 1                                                                 

        for idx, bird in enumerate(birds): 
            ge[idx].fitness += 0.1
            bird.move()

            
            output = nns[birds.index(bird)].activate((bird.pos_y, abs(bird.pos_y - pipes[pipe_idx].height), abs(bird.pos_y - pipes[pipe_idx].bottom)))

            if output[0] > 0.5:  
                bird.jump()

        base.move()

        rem = []
        new_pipe = False
        for pipe in pipes:
            pipe.move()
            
            for bird in birds:
                if pipe.collide(bird, win):
                    ge[birds.index(bird)].fitness -= 1
                    nns.pop(birds.index(bird))
                    ge.pop(birds.index(bird))
                    birds.pop(birds.index(bird))

            if pipe.x + pipe.P_TOP.get_width() < 0:
                rem.append(pipe)

            if not pipe.passed and pipe.x < bird.pos_x:
                pipe.passed = True
                new_pipe = True

        if new_pipe:
            score += 1
            
            for genome in ge:
                genome.fitness += 5
            pipes.append(Pipe(WINDOW_W))

        for r in rem:
            pipes.remove(r)

        for bird in birds:
            if bird.pos_y + bird.img.get_height() - 10 >= F_BASE or bird.pos_y < -50:
                nns.pop(birds.index(bird))
                ge.pop(birds.index(bird))
                birds.pop(birds.index(bird))

        fb.draw_window(win, birds, pipes, base,pipe_idx,score,gen)




def run(config_file):

    config = neat.config.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_file)


    pop = neat.Population(config)

    pop.add_reporter(neat.StdOutReporter(True))
    data = neat.StatisticsReporter()
    pop.add_reporter(data)

    winner = pop.run(flappy_fitness, 50)

   


# if __name__ == '__main__':

local_dir = os.path.dirname("")
config_path = os.path.join(local_dir, 'config-neat.txt')
run(config_path)







