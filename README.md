# CS7IS2 AI Group Project
GROUP   : muraleeu@tcd.ie, joyj@tcd.ie, makarana@tcd.ie, thomasto@tcd.ie, jayapraa@tcd.ie

TO DO   : https://docs.google.com/spreadsheets/d/1IkubXXeWJz19PkWZbUdl33mWfON4IbmQ-YLxTIcfCEk/edit?usp=sharing

REPORT  : https://docs.google.com/document/d/1mLeez1RUtHHQTII26MViH3h6cWi35VQTXpdrCKcPSek/edit?usp=sharing

## Contributions

-  Implementation of neat Algorithm - thomasto@tcd.ie
-  Implementation of A3C Algorithm - jayapraa@tcd.ie,makarana@tcd.ie
-  Implementation of MIP - joyj@tcd.ie
-  Implementation of Q- Learning - muraleeu@tcd.ie

Presentation Link - https://drive.google.com/drive/folders/12fJ37bxyoLXkFYG8aG3PwFPxp-P8sYL2?usp=sharing

Unnikrishnan Muraleedharan	21337857    <muraleeu@tcd.ie>
Azin Makaranth                  21334051    <makarana@tcd.ie>
John Joy                        21331882    <joyj@tcd.ie>
Tom Mathew Thomas             	21334296    <thomasto@tcd.ie>
Arun Jayaprakash                21333442    <jayapraa@tcd.ie>

GitLab Repository : https://gitlab.scss.tcd.ie/jayapraa/cs7is2-ai-group-project


Summary of Contributions by each team member

Arun Jayaprakash
Myself and Azin worked collaboratively in setting up A3C. I developed the neural network used in the algorithm, and we both collectively worked on the code for the function that updates the global loss using this neural network. I also worked on the report for the same, and helped in the final proof-reading. Initially I also spent considerable time researching various approaches and identifying feasibility.

Azin Makaranth
I worked with Arun on A3C algorithm. Due to the significant complexity of the algorithm, we split up various parts of the algorithm between us 2. I worked with Arun on setting up a function that calculates the advantage from the neural network outputs which was backpropagated to the network for further training.
I also worked on setting up a code to test the algorithm by keeping track of the top score for 3 runs for various models (varying episode lengths) and worked on the evaluation part of the report for the same.

John Joy
I implemented MIP using cvxpy and gurobi optimizer by giving the input as position of the bird, velocity of the bird and the pipe positions. The optimisation problem which has various interger constraints like sky, ground etc in the game play, acceleration , velocity are considered to return the output of the flap or no flap. This is implemented using a a branch and bound method which is the fast optimiser problem for a linear inequality optimisation solving.

Tom Mathew Thomas
I set up the initial code base. Initially I spent time resaerching various algorithms and identified that NEAT would be an interesting approach to the problem. I implemented NEAT using neat-python packages and optimized various neat configuarations and worked on the report for the same.

Unnikrishnan Muraleedharan
I implemented Q-Learning algorithm in Flappy Bird and used the game environment developed by Tom for the same. I also worked on the report for the same and maintained meeting minutes and the todo list. I was responsible for creating the final presentation for the project.

Common contributions:
All team members explored various algorithms and converged towards 4 final approaches
All team members contributed to the final report to the same extent and worked together for final evaluation and comparison
