#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pygame
import random
import os
import time
import neat
import numpy as np
import FlappyBird as fb


# In[2]:


WINDOW_W = fb.WINDOW_W
WINDOW_H = fb.WINDOW_H
Bird = fb.Bird
Pipe = fb.Pipe
Base = fb.Base
F_BASE = fb.F_BASE
EV_IMG = fb.EV_IMG


# In[3]:


pygame.font.init()
Font = pygame.font.SysFont("comicsans",18)




Q_val = np.zeros((288,500,2))


# In[ ]:





# In[ ]:





# In[5]:


def get_Env_Measures(bird,pipe):
    x_val = pipe.x - bird.pos_x
    y_val = bird.pos_y - pipe.height + 50
    if y_val < 0:
        y_val = abs(y_val) + 100

    return int(x_val/25), int(y_val/25)


# In[6]:


def get_q_action(x,y):
    val = 1
    if Q_val[x][y][0] > Q_val[x][y][1]:
        val = 0
    return val


# In[7]:


def Q_val_update(x,y,action,x_nxt,y_nxt,reward):
    Q_val[x][y][action] = 0.4* Q_val[x][y][action] + 0.6*(reward + max(Q_val[x_nxt][y_nxt][0],Q_val[x_nxt][y_nxt][1]))


# In[8]:


episodes = 0
win = pygame.display.set_mode((WINDOW_W, WINDOW_H))
def q_bot_play():
    global episodes
    bird = Bird(WINDOW_W/4,WINDOW_H/2)
    pipes = [Pipe(WINDOW_W)]
    base = Base(F_BASE)
    score = 0
    clock = pygame.time.Clock()
    collide = False
    print(bird)
    win = pygame.display.set_mode((WINDOW_W, WINDOW_H))
    while not collide:
        clock.tick(30)
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
                quit()
                break

        pipe_idx = 0
        
        if len(pipes) > 1 and bird.pos_x > pipes[0].x + pipes[0].P_TOP.get_width():  
            pipe_idx = 1
        
        x, y = get_Env_Measures(bird,pipes[pipe_idx])
        
        action = get_q_action(x,y)
        
        if action:
            bird.jump()
        
        base.move()
        bird.move()
        reward = 15
        rem = []

        new_pipe = False
        for pipe in pipes:
            pipe.move()
            
            
            if pipe.collide(bird, win):
                reward = -1000
                collide = True
                break

            if pipe.x + pipe.P_TOP.get_width() < 0:
                rem.append(pipe)

            if not pipe.passed and pipe.x < bird.pos_x:
                pipe.passed = True
                new_pipe = True
        if bird.pos_y + bird.img.get_height() - 10 >= F_BASE or bird.pos_y < -5:
            reward = -1000
            collide = True
            
        if new_pipe:
            score += 1
            # can add this line to give more reward for passing through a pipe (not required)
            
            pipes.append(Pipe(WINDOW_W))
        x_new , y_new  = get_Env_Measures(bird,pipes[pipe_idx])
        Q_val_update(x,y,action,x_new,y_new,reward)
        
        for r in rem:
            pipes.remove(r)
         
      
        win.blit(EV_IMG,(0,0))
        bird.draw(win)
        for pipe in pipes:
            pipe.draw(win)
        
        base.draw(win)
       
        bird.draw(win) 
        s = Font.render("Score: "+ str(score),1,(255,255,255))
        g = Font.render("Episodes: "+ str(episodes),1,(255,255,255))
        win.blit(s,(WINDOW_W - 15 -s.get_width(),10))
        win.blit(g,(WINDOW_W - 50 -s.get_width(),30))
        pygame.display.update()
    
    return score


# In[9]:


def q_learning_agent(max_score):
    score = 0
    global episodes
    while score < max_score:
        episodes+=1
        score =  q_bot_play()


# In[10]:


q_learning_agent(25)


# In[ ]:




