#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pygame
import random
import os
import time


# In[2]:

pygame.font.init()
Font = pygame.font.SysFont("comicsans",18)
WINDOW_W = 288
WINDOW_H = 405
F_BASE = 390
FB_IMG = [pygame.image.load(os.path.join("game_images","bird1.png")),pygame.image.load(os.path.join("game_images","bird2.png")),pygame.image.load(os.path.join("game_images","bird3.png"))]
PIPE_IMG = pygame.image.load(os.path.join("game_images","pipe.png"))
EV_IMG = pygame.image.load(os.path.join("game_images","bg.png"))
GROUND_IMG = pygame.image.load(os.path.join("game_images","base.png"))


# In[3]:


class Bird:
    
    ANIM_T = 5
    
    
    def __init__(self, x, y):
        self.pos_x = x
        self.pos_y = y
        self.cur_img = 0
        self.img = FB_IMG[0]
        self.h = y
        self.velocity = 0
        self.j_counter = 0
    
    def jump(self):
        self.velocity = -5.5
        self.h = self.pos_y
        self.j_counter = 0
        
    def move(self):
        self.j_counter += 1
        displacement = self.velocity*(self.j_counter) + 0.5*(3)*(self.j_counter)**2  
        if displacement >= 16:
            displacement =  16
        if displacement < 0:
            displacement -= 2
        self.pos_y = self.pos_y + displacement
       
    def draw(self, win):
        self.cur_img += 1

        # For animation of bird, loop through three images
        if self.cur_img <= self.ANIM_T:
            self.img = FB_IMG[0]
        elif self.cur_img <= self.ANIM_T*2:
            self.img = FB_IMG[1]
        elif self.cur_img <= self.ANIM_T*3:
            self.img = FB_IMG[2]
        elif self.cur_img <= self.ANIM_T*4:
            self.img = FB_IMG[1]
        elif self.cur_img == self.ANIM_T*4 + 1:
            self.img = FB_IMG[0]
            self.cur_img = 0

        win.blit(self.img, (self.pos_x, self.pos_y))

    def get_mask(self):
        
        return pygame.mask.from_surface(self.img)


# In[4]:


class Pipe():
   
    P_GAP = 100
    MOVE_VEL = 5

    def __init__(self, x):
     
        self.x = x
        self.height = 0

        self.top = 0
        self.bottom = 0

        self.P_TOP = pygame.transform.flip(PIPE_IMG, False, True)
        self.P_BOTTOM = PIPE_IMG

        self.passed = False

        self.set_height()

    def set_height(self):
       
        self.height = random.randrange(20, WINDOW_H/3)
        self.top = self.height - self.P_TOP.get_height()
        self.bottom = self.height + self.P_GAP

    def move(self):
        
        self.x -= self.MOVE_VEL

    def draw(self, win):

        win.blit(self.P_TOP, (self.x, self.top))

        win.blit(self.P_BOTTOM, (self.x, self.bottom))


    def collide(self, bird, win):
        bird_mask = bird.get_mask()
        top_mask = pygame.mask.from_surface(self.P_TOP)
        bottom_mask = pygame.mask.from_surface(self.P_BOTTOM)
        top_offset = (self.x - bird.pos_x, self.top - round(bird.pos_y))
        bottom_offset = (self.x - bird.pos_x, self.bottom - round(bird.pos_y))

        b_point = bird_mask.overlap(bottom_mask, bottom_offset)
        t_point = bird_mask.overlap(top_mask,top_offset)

        if b_point or t_point:
            return True

        return False


# In[5]:


class Base:
    MOVE_VEL = 5
    WIDTH = GROUND_IMG.get_width()
    IMG = GROUND_IMG

    def __init__(self, y):
        self.y = y
        self.x1 = 0
        self.x2 = self.WIDTH

    def move(self):
        self.x1 -= self.MOVE_VEL
        self.x2 -= self.MOVE_VEL
        if self.x1 + self.WIDTH < 0:
            self.x1 = self.x2 + self.WIDTH

        if self.x2 + self.WIDTH < 0:
            self.x2 = self.x1 + self.WIDTH

    def draw(self, win):
     
        win.blit(self.IMG, (self.x1, self.y))
        win.blit(self.IMG, (self.x2, self.y))


# In[6]:


def draw_window(win,birds,pipes,base,pipe_idx,sc,gn):
    win.blit(EV_IMG,(0,0))
    for pipe in pipes:
        pipe.draw(win)
    base.draw(win)
    for bird in birds:

        for bird in birds:
        # draw lines from bird to pipe
            
            try:
                b_x = bird.pos_x
                b_y = bird.pos_y
                p_x = pipes[pipe_idx].x
                p_y =  pipes[pipe_idx].height
                p_y2 = pipes[pipe_idx].bottom
                pygame.draw.line(win, (255,0,0), (b_x,b_y), (p_x,p_y), 5)
                pygame.draw.line(win, (255,0,0), (b_x,b_y), (p_x,p_y2), 5)
            except:
                pass


            bird.draw(win)
        
    s = Font.render("Score: "+ str(sc),1,(255,255,255))
    b = Font.render("Population:"+str(len(birds)),1,(255,255,255))
    g = Font.render("Generation: "+ str(gn),1,(255,255,255))
    win.blit(s,(WINDOW_W - 15 -s.get_width(),10))
    win.blit(b,(WINDOW_W - 50 -s.get_width(),30))
    win.blit(g,(WINDOW_W - 50 -s.get_width(),50))
    pygame.display.update()


# In[ ]:




