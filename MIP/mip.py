import cvxpy as cvx
import numpy as np
import matplotlib.pyplot as plt


N = 24 
path = cvx.Variable((N, 2)) 
flap = cvx.Variable(N-1, boolean=True) 
res = [False, False, False] 
endPath = [(0,0),(0,0)] 

fast_optimizer = "GUROBI"

gap  = 100 
pipeWd = 52
birdWd = 34
birdHt = 24
dia = np.sqrt(birdHt**2 + birdWd**2) 
ceil = 0 
floor = (512*0.80)-1
playx = 57


def pipeConstraints(x, y, pipe):
    constraints = [] 
    dist = 0 
    for pipe in pipe:
        firstDist = pipe['x'] - x - dia
        endDist = pipe['x'] - x + pipeWd
        if (firstDist < 0) and (endDist > 0):
            constraints += [y <= (pipe['y'] - dia)] 
            constraints += [y >= (pipe['y'] - gap)] 
            dist += cvx.abs(pipe['y'] - (gap//2) - (dia//2) - y) 
    return constraints, dist

def solve(positionY, velocityY, pipe):

    pipeVelocityX = -4 
    acc    =   1   
    flapacc =  -14  


    y = path[:,0]
    vy = path[:,1]

    constraint = [] 
    constraint += [y <= floor, y >= ceil] 
    constraint += [y[0] == positionY, vy[0] == velocityY] 

    ob = 0
    
    x = playx
    xs = [x] 
    for t in range(N-1): 
        dt = t//14 + 1 
        x -= dt * pipeVelocityX 
        xs += [x] 
        constraint += [vy[t + 1] ==  vy[t] + acc * dt + flapacc * flap[t] ] 
        constraint += [y[t + 1] ==  y[t] + vy[t + 1]*dt ] 
        pipe_c, dist = pipeConstraints(x, y[t+1], pipe) 
        constraint += pipe_c
        ob += dist
    


    objective = cvx.Minimize(cvx.sum(cvx.abs(vy)) + 100* ob)

    prob = cvx.Problem(objective, constraint) 
    try:
        
        prob.solve(verbose = False, solver=fast_optimizer) 
        endPath = list(zip(xs, y.value)) 
        res = np.round(flap.value).astype(bool) 
        return res[0], endPath 
    except:
        try:
            res = res[1:] 
            endPath = [((x-4), y) for (x,y) in endPath[1:]]
            return res[0], endPath
        except:
            return False, [(0,0), (0,0)] 

