import numpy as np
import sys
sys.path.append("game/")
import skimage
from skimage import transform, color, exposure
import keras
from keras.models import Sequential, Model, load_model
from keras.layers import Dense, Flatten, Activation, Input
from keras.layers.convolutional import Convolution2D
from keras.optimizers import RMSprop
import keras.backend as K
from keras.callbacks import LearningRateScheduler, History
import tensorflow as tf
import pandas as pd
import pygame
import wrapped_flappy_bird as game
from datetime import datetime
import threading


tf_sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
GAMMA = 0.99
BETA = 0.01
rows = 85
cols = 84
channels = 4
LEARNING_RATE = 7e-4
episode = 0
THREADS = 16
t_max = 5  
const = 1e-5
T = 0

episode_r = []
episode_state = np.zeros((0, rows, cols, channels))
episode_output = []
episode_critic = []

POSSIBLE_ACTIONS = 2
a_t = np.zeros(POSSIBLE_ACTIONS)


# AGENT POLICY LOSS FUNCTION
def log_loss_policy(y_true, y_pred):
	return -K.sum( K.log(y_true*y_pred + (1-y_true)*(1-y_pred) + const), axis=-1) 


# CRITIC LOSS FUNCTION
def sum_of_squares_critic(y_true, y_pred):
	return K.sum(K.square(y_pred - y_true), axis=-1)


# NEURAL NETWORK MODEL
def nn_model():
	model = Sequential()
	keras.initializers.RandomUniform(minval=-0.1, maxval=0.1, seed=None)

	S = Input(shape = (rows, cols, channels,), name ='Input')
	CONV_LAYER_1 = Convolution2D(16, kernel_size = (8, 8), strides = (4, 4), activation ='relu', kernel_initializer ='random_uniform', bias_initializer ='random_uniform')(S)
	CONV_LAYER_2 = Convolution2D(32, kernel_size = (4, 4), strides = (2, 2), activation ='relu', kernel_initializer ='random_uniform', bias_initializer ='random_uniform')(CONV_LAYER_1)
	FLATTTEN_LAYER = Flatten()(CONV_LAYER_2)
	DENSE_LAYER = Dense(256, activation ='relu', kernel_initializer ='random_uniform', bias_initializer ='random_uniform') (FLATTTEN_LAYER)
	POLICY_OUT = Dense(1, name ='o_P', activation ='sigmoid', kernel_initializer ='random_uniform', bias_initializer ='random_uniform') (DENSE_LAYER)
	VALUE_OUT = Dense(1, name ='o_V', kernel_initializer ='random_uniform', bias_initializer ='random_uniform') (DENSE_LAYER)

	model = Model(inputs = S, outputs = [POLICY_OUT, VALUE_OUT])
	rms = RMSprop(lr = LEARNING_RATE, rho = 0.99, epsilon = 0.1)
	model.compile(loss = {'o_P': log_loss_policy, 'o_V': sum_of_squares_critic}, loss_weights = {'o_P': 1., 'o_V' : 0.5}, optimizer = rms)
	return model

# GRAYSCALE and resize array
def preproc_img(image):
	image = skimage.color.rgb2gray(image)
	image = skimage.transform.resize(image, (rows, cols), mode ='constant')
	image = skimage.exposure.rescale_intensity(image, out_range=(0,255))
	image = image.reshape(1, image.shape[0], image.shape[1], 1)
	return image


model = nn_model()
model._make_predict_function()
graph = tf.get_default_graph()

interim_layer = Model(inputs=model.input, outputs=model.get_layer('o_P').output)


a_t[0] = 1

game_state = []
for i in range(0, THREADS):
	game_state.append(game.GameState(30000))


def process_frame(thread_id, state_t):
	global T
	global a_t
	global model

	t = 0
	t_start = t
	terminal = False
	reward_t = 0
	reward_array = []
	state_store = np.zeros((0, rows, cols, channels))
	output_store = []
	critic_store = []
	state_t = state_t.reshape(1, state_t.shape[0], state_t.shape[1], state_t.shape[2])

	while t-t_start < t_max and terminal == False:
		t += 1
		T += 1
		intermediate_output = 0	

		with graph.as_default():
			out = model.predict(state_t)[0]
			intermediate_output = interim_layer.predict(state_t)
		no = np.random.rand()
		a_t = [0, 1] if no < out else [1, 0]

		x_t, reward_t, terminal = game_state[thread_id].frame_step(a_t)
		x_t = preproc_img(x_t)

		with graph.as_default():
			critic_reward = model.predict(state_t)[1]

		y = 0 if a_t[0] == 1 else 1

		reward_array = np.append(reward_array, reward_t)
		state_store = np.append(state_store, state_t, axis = 0)
		output_store = np.append(output_store, y)
		critic_store = np.append(critic_store, critic_reward)
		
		state_t = np.append(x_t, state_t[:, :, :, :3], axis=3)
		print("Frame = " + str(T) + ", Updates = " + str(episode) + ", Thread = " + str(thread_id) + ", Output = " + str(intermediate_output))
	
	if terminal == False:
		reward_array[len(reward_array) - 1] = critic_store[len(reward_array) - 1]
	else:
		reward_array[len(reward_array) - 1] = -1
		state_t = np.concatenate((x_t, x_t, x_t, x_t), axis=3)
	
	for i in range(2, len(reward_array) + 1):
		reward_array[len(reward_array) - i] = reward_array[len(reward_array) - i] + GAMMA * reward_array[len(reward_array) - i + 1]

	return state_t, state_store, output_store, reward_array, critic_store

#function to decrease the learning rate after every epoch. In this manner, the learning rate reaches 0, by 20,000 epochs
def step_decay(epoch):
	decay = 3.2e-8
	lrate = LEARNING_RATE - epoch*decay
	lrate = max(lrate, 0)
	return lrate

class actorthread(threading.Thread):
	def __init__(self,thread_id, s_t):
		threading.Thread.__init__(self)
		self.thread_id = thread_id
		self.next_state = s_t

	def run(self):
		global episode_output
		global episode_r
		global episode_critic
		global episode_state

		threadLock.acquire()
		self.next_state, state_store, output_store, r_store, critic_store = process_frame(self.thread_id, self.next_state)
		self.next_state = self.next_state.reshape(self.next_state.shape[1], self.next_state.shape[2], self.next_state.shape[3])

		episode_r = np.append(episode_r, r_store)
		episode_output = np.append(episode_output, output_store)
		episode_state = np.append(episode_state, state_store, axis = 0)
		episode_critic = np.append(episode_critic, critic_store)

		threadLock.release()

states = np.zeros((0, rows, cols, 4))

#initializing state of each thread
for i in range(0, len(game_state)):
	image = game_state[i].getCurrentFrame()
	image = preproc_img(image)
	state = np.concatenate((image, image, image, image), axis=3)
	states = np.append(states, state, axis = 0)


# df_rewards = pd.DataFrame(columns=['Episode', 'Mean_Reward', 'Loss'])
data = {'Episode': [], 'Mean_Reward': [], 'Loss': []}
while True:
	if episode == 20000:
		break
	threadLock = threading.Lock()
	threads = []
	for i in range(0,THREADS):
		threads.append(actorthread(i,states[i]))

	states = np.zeros((0, rows, cols, 4))

	for i in range(0,THREADS):
		threads[i].start()

	#thread.join() ensures that all threads fininsh execution before proceeding further
	for i in range(0, THREADS):
		threads[i].join()

	for i in range(0, THREADS):
		state = threads[i].next_state
		state = state.reshape(1, state.shape[0], state.shape[1], state.shape[2])
		states = np.append(states, state, axis = 0)

	e_mean = np.mean(episode_r)

	advantage = episode_r - episode_critic

	alpha_upd = LearningRateScheduler(step_decay)
	callbacks_list = [alpha_upd]

	weights = {'o_P':advantage, 'o_V':np.ones(len(advantage))}	


	history = model.fit(episode_state, [episode_output, episode_r], epochs =episode + 1, batch_size = len(episode_output), callbacks = callbacks_list, sample_weight = weights, initial_epoch = episode)

	episode_r = []
	episode_output = []
	episode_state = np.zeros((0, rows, cols, channels))
	episode_critic = []

	f = open("rewards.txt","a")
	f.write("Update: " + str(episode) + ", Reward_mean: " + str(e_mean) + ", Loss: " + str(history.history['loss']) + "\n")
	f.close()
	data['Episode'].append(episode)
	data['Mean_Reward'].append(e_mean)
	data['Loss'].append(history.history['loss'][0])

	if episode % 50 == 0:
		model.save("saved_models/model_updates" + str(episode))
	episode += 1


metrics_df = pd.DataFrame.from_dict(data)
t = str(datetime.now())
t = t.replace(".", "_")
t = t.replace(":", "_")
metrics_df.to_csv("metrics/train/Train_" + t + ".csv")
