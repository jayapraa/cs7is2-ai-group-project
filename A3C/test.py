import numpy as np 
import sys
sys.path.append("game/")
import pygame
import pandas as pd
import skimage
from datetime import datetime
from skimage import transform, color, exposure
import keras
from keras.models import Sequential, Model, load_model
from keras.layers.core import Dense, Flatten, Activation
from keras.layers.convolutional import Convolution2D
from keras.optimizers import RMSprop
import keras.backend as keras_backend
import time
import wrapped_flappy_bird as game

BETA = 0.01
const = 1e-5

# AGENT POLICY LOSS FUNCTION
def log_loss_policy(y_true, y_pred):
	return -keras_backend.sum(keras_backend.log(y_true * y_pred + (1 - y_true) * (1 - y_pred) + const), axis=-1)

# CRITIC LOSS FUNCTION
def sum_of_squares_critic(y_true, y_pred):        #critic loss
	return keras_backend.sum(keras_backend.square(y_pred - y_true), axis=-1)

def preprocess(image):
	image = skimage.color.rgb2gray(image)
	image = skimage.transform.resize(image, (85,84), mode = 'constant')
	image = skimage.exposure.rescale_intensity(image, out_range=(0,255))
	image = image.reshape(1, image.shape[0], image.shape[1], 1)
	return image


#TEST GAME WITH MODEL
def run_game(model_episode):
	current_game_score = 0
	top_game_score = 0
	acceleration_t = [1, 0]
	FIRST_FRAME = True

	terminal = False
	reward_x = 0
	data = {'Trial': [], 'Current_Score': [], 'Top_Score': []}
	trial = 0
	model = load_model("saved_models/model_updates" + str(model_episode),
					   custom_objects={'logloss': log_loss_policy, 'sumofsquares': sum_of_squares_critic})
	game_state = game.GameState(30)
	counter = 0

	while True:
		if counter == 3:
			break

		if top_game_score == 50:
			break

		if FIRST_FRAME:
			pos_x = game_state.getCurrentFrame()
			pos_x = preprocess(pos_x)
			s_t = np.concatenate((pos_x, pos_x, pos_x, pos_x), axis=3)
			FIRST_FRAME = False
		else:
			pos_x, reward_x, terminal = game_state.get_game_frame(acceleration_t)
			pos_x = preprocess(pos_x)
			s_t = np.append(pos_x, s_t[:, :, :, :3], axis=3)

		y = model.predict(s_t)
		no = np.random.random()
		no = np.random.rand()
		acceleration_t = [0, 1] if no < y[0] else [1, 0]

		if reward_x == 1:
			current_game_score += 1
			top_game_score = max(top_game_score, current_game_score)

			print("Current Score: " + str(current_game_score) + " Top Score: " + str(top_game_score))

			data['Trial'].append(trial)
			data['Current_Score'].append(current_game_score)
			data['Top_Score'].append(top_game_score)

		if reward_x == -1:
			print("DED!")
			trial += 1
			data['Trial'].append(trial)
			data['Current_Score'].append(current_game_score)
			data['Top_Score'].append(top_game_score)

		if terminal == True:
			print("TRIAL = ", counter)
			counter += 1
			FIRST_FRAME = True
			terminal = False
			current_game_score = 0

	return top_game_score

for i in (9500,1000):
	print("--------------------MODEL " + str(i) +"----------------")
	top = run_game(i)
	print(top)

	f = open("metrics/test/test_rewards_updated.txt", "a")
	f.write("MODEL_" + str(i) + " " + str(top) + "\n")
	f.close()


# metrics_df = pd.DataFrame.from_dict(data)
# t = str(datetime.now())
# t = t.replace(".", "_")
# t = t.replace(":", "_")

# metrics_df.to_csv("metrics/test/Test_" + t + ".csv")
