import visualkeras
from keras.models import Sequential, Model, load_model
import keras.backend as keras_backend
BETA = 0.01
const = 1e-5
# AGENT POLICY LOSS FUNCTION
def log_loss_policy(y_true, y_pred):
	return -keras_backend.sum(keras_backend.log(y_true * y_pred + (1 - y_true) * (1 - y_pred) + const), axis=-1)

# CRITIC LOSS FUNCTION
def sum_of_squares_critic(y_true, y_pred):        #critic loss
	return keras_backend.sum(keras_backend.square(y_pred - y_true), axis=-1)
model = load_model("saved_models/model_updates4500",
						   custom_objects={'logloss': log_loss_policy, 'sumofsquares': sum_of_squares_critic})

visualkeras.layered_view(model).show()
visualkeras.layered_view(model, legend=True, to_file='output.png')
visualkeras.layered_view(model, legend=True, to_file='output.png').show()

visualkeras.layered_view(model)